package com.mohamedkhammeri.selfawareapp.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mohamedkhammeri.selfawareapp.models.UserEntity;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM userentity")
    List<UserEntity> getAll();

    @Query("SELECT * FROM userentity WHERE id = :id")
    UserEntity findById(int id);

    @Insert
    void insertUser(UserEntity users);

    @Delete
    void delete(UserEntity user);
}
