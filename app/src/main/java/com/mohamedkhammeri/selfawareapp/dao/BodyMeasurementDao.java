package com.mohamedkhammeri.selfawareapp.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mohamedkhammeri.selfawareapp.models.BodyMeasurement;
import com.mohamedkhammeri.selfawareapp.models.UserEntity;

import java.util.List;

@Dao
public interface BodyMeasurementDao {

    @Query("SELECT * FROM bodymeasurement")
    List<BodyMeasurement> getAll();

    @Insert
    void insertBodyMeasurement(BodyMeasurement bodyMeasurement);

    @Update
    void updateBodyMeasurement(BodyMeasurement bodyMeasurement);

    @Delete
    void delete(BodyMeasurement bodyMeasurement);
}
