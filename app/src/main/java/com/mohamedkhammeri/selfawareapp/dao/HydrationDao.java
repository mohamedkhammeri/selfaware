package com.mohamedkhammeri.selfawareapp.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mohamedkhammeri.selfawareapp.models.UserHydration;

import java.util.List;

@Dao
public interface HydrationDao {

    @Query("SELECT * FROM userhydration")
    List<UserHydration> getAll();

    @Insert
    void insertHydration(UserHydration userHydration);

    @Update
    void updateHydration(UserHydration userHydration);

    @Delete
    void delete(UserHydration userHydration);
}
