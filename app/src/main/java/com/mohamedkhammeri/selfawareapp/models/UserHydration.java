package com.mohamedkhammeri.selfawareapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class UserHydration {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "hydration_goal")
    private int hydrationGoal;

    @ColumnInfo(name = "hydration_progress")
    private int hydrationProgress;

    public UserHydration() {
        this.hydrationGoal = 0;
        this.hydrationProgress = 0;
    }

    @Ignore
    public UserHydration(int hydrationGoal, int hydrationProgress) {
        this.hydrationGoal = hydrationGoal;
        this.hydrationProgress = hydrationProgress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHydrationGoal() {
        return hydrationGoal;
    }

    public void setHydrationGoal(int hydrationGoal) {
        this.hydrationGoal = hydrationGoal;
    }

    public int getHydrationProgress() {
        return hydrationProgress;
    }

    public void setHydrationProgress(int hydrationProgress) {
        this.hydrationProgress = hydrationProgress;
    }

    @Override
    public String toString() {
        return "UserHydration{" +
                "id=" + id +
                ", hydrationGoal=" + hydrationGoal +
                ", hydrationProgress=" + hydrationProgress +
                '}';
    }
}
