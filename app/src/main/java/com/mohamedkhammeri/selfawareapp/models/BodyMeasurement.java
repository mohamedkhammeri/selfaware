package com.mohamedkhammeri.selfawareapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.DecimalFormat;

@Entity
public class BodyMeasurement {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "weight")
    private int weight;

    @ColumnInfo(name = "height")
    private int height;

    @ColumnInfo(name = "bmi")
    private double bmi;

    public BodyMeasurement() {
        this.weight = 0;
        this.height = 0;
        this.bmi = 0;

    }
    @Ignore
    public BodyMeasurement(int weight, int height) {
        this.weight = weight;
        this.height = height;
        this.bmi = calculateBMI();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }


    @Override
    public String toString() {
        return "BodyMeasurement{" +
                "id=" + id +
                ", weight=" + weight +
                ", height=" + height +
                ", bmi=" + bmi +
                '}';
    }

    public double calculateBMI(){
        double bmi;
        double heightSquare;
        if(this.weight == 0)
            heightSquare = 1;
        else {
            float heightMeter = this.height *  1.0f / 100;
            heightSquare = Math.pow(heightMeter, 2d);
        }

        bmi = this.weight / heightSquare;
        DecimalFormat df = new DecimalFormat("####0.00");
        double finalBMI = Double.parseDouble(df.format(bmi));
        this.bmi = finalBMI;
        return finalBMI;
    }
}
