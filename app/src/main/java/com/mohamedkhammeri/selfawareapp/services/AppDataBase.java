package com.mohamedkhammeri.selfawareapp.services;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mohamedkhammeri.selfawareapp.dao.BodyMeasurementDao;
import com.mohamedkhammeri.selfawareapp.dao.HydrationDao;
import com.mohamedkhammeri.selfawareapp.models.BodyMeasurement;
import com.mohamedkhammeri.selfawareapp.models.UserEntity;
import com.mohamedkhammeri.selfawareapp.dao.UserDao;
import com.mohamedkhammeri.selfawareapp.models.UserHydration;

@Database(entities = {UserEntity.class, BodyMeasurement.class, UserHydration.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase  {
    public static String DB_NAME = "selfAware_db";
    public abstract UserDao userDao();
    public abstract BodyMeasurementDao bodyMeasurementDao();
    public abstract HydrationDao hydrationDao();
    public static AppDataBase instance;

    public static synchronized AppDataBase getInstance(Context context){
        if(instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDataBase.class, DB_NAME)
                    .allowMainThreadQueries().build();

        return instance;
    }
}
