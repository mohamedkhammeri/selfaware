package com.mohamedkhammeri.selfawareapp.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.mohamedkhammeri.selfawareapp.ApplicationClass;

public class SharedPreferenceHelper {


    private static final String TAG = SharedPreferenceHelper.class.getSimpleName();
    private SharedPreferences prefs;

    public SharedPreferenceHelper() {
        this.prefs = ApplicationClass.getContext().getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE);
    }

    public void insertString(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    public void removeSharedPref(String key){
        prefs.edit().remove(key).apply();
    }

    public void insertLong(String key, long value) {
        prefs.edit().putLong(key, value).apply();
    }

    public long getLong(String key) {
        return prefs.getLong(key, 0);
    }

    public void insertBool(String key, boolean value) {
        prefs.edit().putBoolean(key, value).apply();
    }

    public boolean getBool(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    public void insertInt(String key, int value) {
        prefs.edit().putInt(key, value).apply();
    }


    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

    public String getString(String key) {
        return prefs.getString(key, "");
    }

    public  String getString(String key, String defaultOne) {
        return prefs.getString(key, defaultOne);
    }
}