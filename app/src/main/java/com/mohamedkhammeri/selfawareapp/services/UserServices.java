package com.mohamedkhammeri.selfawareapp.services;

import android.content.Context;

import com.mohamedkhammeri.selfawareapp.dao.BodyMeasurementDao;
import com.mohamedkhammeri.selfawareapp.dao.HydrationDao;
import com.mohamedkhammeri.selfawareapp.models.BodyMeasurement;
import com.mohamedkhammeri.selfawareapp.models.UserEntity;
import com.mohamedkhammeri.selfawareapp.models.UserHydration;
import com.mohamedkhammeri.selfawareapp.services.callbacks.CallBacks;
import com.mohamedkhammeri.selfawareapp.dao.UserDao;

public class UserServices {
    private final CallBacks callBacks;
    private UserDao userDao;
    private BodyMeasurementDao bodyMeasurementDao;
    private HydrationDao hydrationDao;
    public UserServices(Context context, CallBacks callBacks) {
        this.callBacks = callBacks;
        userDao = AppDataBase.getInstance(context).userDao();
        bodyMeasurementDao = AppDataBase.getInstance(context).bodyMeasurementDao();
        hydrationDao = AppDataBase.getInstance(context).hydrationDao();
    }
    public void getAllUsers(){
        callBacks.getAllUsers(userDao.getAll());
    }
    public void insertUser(UserEntity user){
        userDao.insertUser(user);
    }
    public void getBodyMeasurement(){
        callBacks.getBodyMeasurement(bodyMeasurementDao.getAll());
    }
    public void insertBodyMeasurement(BodyMeasurement bodyMeasurement){
        bodyMeasurementDao.insertBodyMeasurement(bodyMeasurement);
    }
    public void getHydration(){
        callBacks.getHydration(hydrationDao.getAll());
    }
    public void updateBodyMeasurement(BodyMeasurement bodyMeasurement){
        bodyMeasurementDao.updateBodyMeasurement(bodyMeasurement);
    }
    public void insertHydration(UserHydration userHydration){
        hydrationDao.insertHydration(userHydration);
    }
    public void updateHydration(UserHydration userHydration){
        hydrationDao.updateHydration(userHydration);
    }
    public void initValues(){
        getBodyMeasurement();
        getHydration();
    }
}
