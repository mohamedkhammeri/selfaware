package com.mohamedkhammeri.selfawareapp.services.callbacks;

import com.mohamedkhammeri.selfawareapp.models.BodyMeasurement;
import com.mohamedkhammeri.selfawareapp.models.UserEntity;
import com.mohamedkhammeri.selfawareapp.models.UserHydration;

import java.util.List;

public interface CallBacks {
    void getAllUsers(List<UserEntity> usersList);
    void getBodyMeasurement(List<BodyMeasurement> bodyMeasurement);
    void getHydration(List<UserHydration> userHydrationList);
}
