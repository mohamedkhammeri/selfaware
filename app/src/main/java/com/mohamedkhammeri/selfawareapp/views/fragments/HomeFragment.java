package com.mohamedkhammeri.selfawareapp.views.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mohamedkhammeri.selfawareapp.R;
import com.mohamedkhammeri.selfawareapp.models.BodyMeasurement;
import com.mohamedkhammeri.selfawareapp.models.UserEntity;
import com.mohamedkhammeri.selfawareapp.models.UserHydration;
import com.mohamedkhammeri.selfawareapp.services.SharedPreferenceHelper;
import com.mohamedkhammeri.selfawareapp.services.UserServices;
import com.mohamedkhammeri.selfawareapp.services.callbacks.CallBacks;

import java.util.List;

public class HomeFragment extends Fragment implements CallBacks {

    private UserServices userServices;
    private SharedPreferenceHelper sharedPreferenceHelper;
    private BodyMeasurement bodyMeasurement;
    private UserHydration userHydration;
    private TextView bmiField;
    private TextView hydrationGoalField;
    private TextView hydrationProgressField;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userServices = new UserServices(getContext(), this);
        sharedPreferenceHelper = new SharedPreferenceHelper();
        userServices.initValues();
        initBMIView(view);
        initHydrationView(view);
    }


    @Override
    public void getAllUsers(List<UserEntity> usersList) {

    }

    @Override
    public void getBodyMeasurement(List<BodyMeasurement> list) {
        if (!list.isEmpty())
            bodyMeasurement = list.get(0);
        else
            userServices.insertBodyMeasurement(new BodyMeasurement());

    }

    @Override
    public void getHydration(List<UserHydration> userHydrationList) {
        if (!userHydrationList.isEmpty())
            userHydration = userHydrationList.get(0);
        else
            userServices.insertHydration(new UserHydration());


    }

    private void updateHydration() {
        if (userHydration != null) {
            hydrationGoalField.setText(userHydration.getHydrationGoal() + "");
            hydrationProgressField.setText(userHydration.getHydrationProgress() + "");
        }
    }

    private void updateBMIValue() {
        if (bodyMeasurement != null && bodyMeasurement.getBmi() != 0d) {
            String bmi = "Your BMI is: " + bodyMeasurement.getBmi();
            bmiField.setText(bmi);
            userServices.updateBodyMeasurement(bodyMeasurement);
        } else
            bmiField.setText("--.--");
    }

    private void initBMIView(View view) {
        bmiField = view.findViewById(R.id.bmi_value);
        updateBMIValue();
        TextView bmiCalculateBtn = view.findViewById(R.id.bmi_calculate_btn);
        bmiCalculateBtn.setOnClickListener(v -> {
            Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.bmi_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            TextView calculate = dialog.findViewById(R.id.dialog_calculate);
            calculate.setOnClickListener(view1 -> {
                String weight = ((EditText) dialog.findViewById(R.id.weight)).getText().toString();
                String height = ((EditText) dialog.findViewById(R.id.height)).getText().toString();
                if (!weight.isEmpty() && !height.isEmpty()) {
                    bodyMeasurement.setWeight(Integer.parseInt(weight));
                    bodyMeasurement.setHeight(Integer.parseInt(height));
                    bodyMeasurement.calculateBMI();
                    updateBMIValue();
                    dialog.dismiss();
                }
            });
            dialog.show();
        });
    }

    private void initHydrationView(View view) {

        hydrationGoalField = view.findViewById(R.id.hydration_goal);
        hydrationProgressField = view.findViewById(R.id.hydration_progress);
        updateHydration();
        TextView hydrationEdit = view.findViewById(R.id.hydration_edit_btn);

        hydrationEdit.setOnClickListener(v -> {
            Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.hydration_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            TextView editBtn = dialog.findViewById(R.id.confirm_button);
            ImageView goalEdit = dialog.findViewById(R.id.edit_hydration_goal);
            TextView hydrationProgress = dialog.findViewById(R.id.hydration_progress);
            EditText goalEditText = dialog.findViewById(R.id.hydration_goal);
            EditText progressEditText = dialog.findViewById(R.id.hydration_progress_text);
            ImageView add = dialog.findViewById(R.id.add_button);
            ImageView remove = dialog.findViewById(R.id.remove_button);
            goalEditText.setText(userHydration.getHydrationGoal() + "");
            hydrationProgress.setText(userHydration.getHydrationProgress() + "");

            add.setOnClickListener(view1 -> {
                String str = progressEditText.getText().toString();
                int progressValue = 0;
                if (!str.isEmpty())
                    progressValue = Integer.parseInt(str);
                progressValue++;
                progressEditText.setText(progressValue + "");
            });

            remove.setOnClickListener(view1 -> {
                String str = progressEditText.getText().toString();
                int progressValue = 0;
                if (!str.isEmpty())
                    progressValue = Integer.parseInt(str);
                if (progressValue > 0)
                    progressValue--;
                progressEditText.setText(progressValue + "");
            });

            progressEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    int progressValue = Integer.parseInt(progressEditText.getText().toString());
                    int finalValue = progressValue + userHydration.getHydrationProgress();
                    hydrationProgress.setText(finalValue + "");
                }
            });

            editBtn.setOnClickListener(view1 -> {

                String goal = goalEditText.getText().toString();
                String progress = hydrationProgress.getText().toString();
                if (!goal.isEmpty() && !progress.isEmpty()) {
                    userHydration.setHydrationGoal(Integer.parseInt(goal));
                    userHydration.setHydrationProgress(Integer.parseInt(progress));
                    userServices.updateHydration(userHydration);
                    updateHydration();
                }
                dialog.dismiss();
            });
            dialog.show();
        });
    }
}