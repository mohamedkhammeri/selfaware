package com.mohamedkhammeri.selfawareapp.views.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mohamedkhammeri.selfawareapp.R;
import com.mohamedkhammeri.selfawareapp.services.SharedPreferenceHelper;
import com.mohamedkhammeri.selfawareapp.services.Utilities;
import com.mohamedkhammeri.selfawareapp.views.MainActivity;

public class SplashScreenFragment extends Fragment {

    public SplashScreenFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_splash_screen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new Handler().postDelayed(() -> {
            SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper();
            boolean isLoggedIn = sharedPreferenceHelper.getBool(Utilities.LOGGED_IN, false);
            Fragment fragment;
            if(isLoggedIn)
                fragment = new HomeFragment();
            else
                fragment = new LoginFragment();
            MainActivity mainActivity = (MainActivity) requireActivity();
            mainActivity.changeFragment(fragment, true, false);
        },2000);
    }

}