package com.mohamedkhammeri.selfawareapp.views.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mohamedkhammeri.selfawareapp.R;
import com.mohamedkhammeri.selfawareapp.models.BodyMeasurement;
import com.mohamedkhammeri.selfawareapp.models.UserEntity;
import com.mohamedkhammeri.selfawareapp.models.UserHydration;
import com.mohamedkhammeri.selfawareapp.services.SharedPreferenceHelper;
import com.mohamedkhammeri.selfawareapp.services.UserServices;
import com.mohamedkhammeri.selfawareapp.services.Utilities;
import com.mohamedkhammeri.selfawareapp.services.callbacks.CallBacks;
import com.mohamedkhammeri.selfawareapp.views.MainActivity;

import java.util.List;

public class LoginFragment extends Fragment implements CallBacks {
    private UserServices services;
    private EditText usernameText, passwordText;
    private SharedPreferenceHelper sharedPreferenceHelper;
    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sharedPreferenceHelper = new SharedPreferenceHelper();
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        usernameText = view.findViewById(R.id.username);
        passwordText = view.findViewById(R.id.password);
        services = new UserServices(getContext(), this);
        Button loginBtn = view.findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(v -> {
            String username = usernameText.getText().toString().trim();
            String password = passwordText.getText().toString().trim();
            if(validateLogin(username, password)){
                services.getAllUsers();
            }
            else
                showToast("Fields should not be empty!");
        });

    }
    private boolean validateLogin(String login, String password){
       return !login.isEmpty() && !password.isEmpty();
    }

    private void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getAllUsers(List<UserEntity> usersList) {
        String username = usernameText.getText().toString().trim();
        String password = passwordText.getText().toString().trim();
        if(usersList.isEmpty()){
            UserEntity user = new UserEntity(username, password);
            services.insertUser(user);
            redirectHomeFragment();
        }
        else{
            for(UserEntity user: usersList){
                if(user.getUsername().equalsIgnoreCase(username) && user.getPassword().equalsIgnoreCase(password)){
                    showToast("Logged in successfully");
                    redirectHomeFragment();
                }else{
                    showToast("Wrong credentials..!");
                }
            }
        }
    }

    @Override
    public void getBodyMeasurement(List<BodyMeasurement> bodyMeasurement) {

    }

    @Override
    public void getHydration(List<UserHydration> userHydrationList) {

    }

    private void redirectHomeFragment(){
        sharedPreferenceHelper.insertBool(Utilities.LOGGED_IN, true);
        MainActivity mainActivity = (MainActivity) requireActivity();
        mainActivity.changeFragment(new HomeFragment(), true, false);
    }

}