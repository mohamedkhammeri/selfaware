package com.mohamedkhammeri.selfawareapp.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.mohamedkhammeri.selfawareapp.R;
import com.mohamedkhammeri.selfawareapp.views.fragments.SplashScreenFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeFragment(new SplashScreenFragment(), false, true);
    }

    public void changeFragment(Fragment fragment, boolean withAnimation, boolean withBackStack){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(withAnimation)
            ft.setCustomAnimations(
                    R.anim.slide_right,
                    R.anim.exit_left);
        ft.add(R.id.main_container, fragment);
        if(withBackStack)
            ft.addToBackStack(null);

        ft.commit();
    }
}