package com.mohamedkhammeri.selfawareapp;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.mohamedkhammeri.selfawareapp.services.AppDataBase;
import com.mohamedkhammeri.selfawareapp.services.Utilities;

public class ApplicationClass extends Application {

    private static ApplicationClass sApplication;

    public static ApplicationClass getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
    }
}
